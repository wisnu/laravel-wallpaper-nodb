@php
	$arr = json_decode($result, true);
@endphp
@extends('main')
@section('title')
	{{ ucfirst($title) }} {{ ucfirst( implode(' ', array_slice($related, 0, 10))) }}
@endsection

@section('meta')
<meta name="description" content="
{{ ucwords(str_replace('-', ' ', $title)) }}
	@for ($i=0; $i<3; $i++)
	{{ ucwords(str_replace('-', ' ', str_slug($arr['data'][$i]['content']))) }},
	@endfor
	">
<meta name="keywords" content="{{ ucwords(str_replace('-', ' ', $title)) }}, {{ ucwords($arr['data'][$i]['content']) }}">
@endsection


@section('content')
<div class="content">
	<div class="article">
		<header class="main-header">
		<div id="header"><a href="{{ url('/') }}" title="{{ ucwords(str_replace('-', ' ', str_slug($arr['data'][$i]['content']))) }}" rel="nofollow"><h1>{{ ucwords(str_replace('-', ' ', str_slug($title))) }}</h1></a></div>
		</header>

		<div class="headertext">
			<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<span typeof="v:Breadcrumb"><a href="{{ url('/') }}" rel="v:url" property="v:title">Home</a></span> »
				<span typeof="v:Breadcrumb"></span><span class="crent">{{ $title }}</span>
			</div>
		</div>

		<div class="ads-top">
			<?=$money['responsiveAds']; //Ads ?>
		</div>
		<p>Showing {{ (($page - 1) * 30 ) + 1 }} - {{ $page * 30 }} of {{ rand(300,700) }} results from {{ $title }} images</p>
		@for ($i=0; $i<99; $i++)

		<div class="box">
			<a href="{{ url('/'.str_slug($title).'/'.$arr['data'][$i]['slug']) }}.html" class="th" title="{{ $arr['data'][$i]['content'] }}" alt="{{ $arr['data'][$i]['content'] }}"><img class="th" onerror="this.onerror=null;this.src='{{ $arr['data'][$i]['tbUrl'] }}';" src="{{ url('img/'.$arr['data'][$i]['slug']) }}.jpg" width="225" height="100" title="{{ $arr['data'][$i]['content'] }}" alt="{{ $arr['data'][$i]['content'] }}" oldsrc="{{ url('img/'.$arr['data'][$i]['slug']) }}.jpg"></a>
			<h2>{{ ucwords($arr['data'][$i]['content']) }}</h2>
		</div>
		@endfor
		<div class="clear"></div>
		<div id="pagination"><a href="{{ url('/'.str_slug($title)) }}" title="First page">« First</a>...<b>1</b>
		@for ($i=2; $i<13; $i++)
			<a href="{{ url('/'.str_slug($title).'/'.$i) }}" title="Page {{$i}}">{{$i}}</a>
		@endfor
		...<a href="{{ url('/'.str_slug($title).'/13') }}" title="Last page">Last »</a></div>

	</div>	<!-- Start Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div id="logo2">
		</div>
	</aside>
	<!-- End Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div class="sidebarmenunavigation">
		</div>
	</aside>


	<aside class="sidebar walleft1">

				<div id="sidebars" class="sidebar">
					<div class="ads_sidebar"><?=$money['responsiveAds']; //Ads ?><!--ads--></div>
					<div class="sidebar_list">
						<ul class="rand-text">
							@foreach ($related as $rel)
								<ul class="popular-posts">
									<li><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}">{{ ucwords($rel) }}</a><div class="sidebartextviews">» {{ rand(1000,3000) }}  views</div></li>
								</ul>
							@endforeach
						</ul>

						<div style="clear: both"></div>
					</div>
				</div>
			</aside>
</div>
@endsection