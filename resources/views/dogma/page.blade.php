@extends('main')
@section('title')
	{{ ucwords(str_replace('-', ' ', $page)) }}
@endsection



@section('content')
<div class="content">
	<div class="article">
		<header class="main-header">
		<div id="header">
			<a href="{{ url('/') }}" title="{{ ucwords(str_replace('-', ' ', $page)) }}" rel="nofollow"><h1>{{ $_SERVER['HTTP_HOST'] }}</h1></a>
		</div>
		</header>
		<div class="headertext">
			<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<span typeof="v:Breadcrumb"><a href="{{ url('/') }}" rel="v:url" property="v:title">Home</a></span> »
				<span typeof="v:Breadcrumb">{{ $page }}</span>
			</div>
		</div>					
		<h1>{{ $page }}</h1>
		<p><?=$content;?></p>
							
	</div>

	<aside class="sidebar walleft1">
		<div id="logo2">
		</div>
	</aside>
	<!-- End Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div class="sidebarmenunavigation">
		</div>
	</aside>


	<aside class="sidebar walleft1">

				<div id="sidebars" class="sidebar">
					<div class="sidebar_list">
						<ul class="rand-text">
							@foreach ($related as $rel)
								<ul class="popular-posts">
									<li><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}">{{ ucwords($rel) }}</a><div class="sidebartextviews">» {{ rand(1000,3000) }}  views</div></li>
								</ul>
							@endforeach
						</ul>

						<div style="clear: both"></div>
					</div>
					<div class="ads_sidebar"><!--ads--></div>
				</div>
			</aside>
</div>
@endsection