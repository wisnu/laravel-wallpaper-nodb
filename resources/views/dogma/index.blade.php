@extends('main')
@section('title')
	{{ ucwords(implode(' ', array_slice($result, 0, 3))) }}
@endsection

@section('meta')
<meta name="description" content="{{ ucwords(implode(' ', array_slice($result, 0, 15))) }}">
<meta name="keywords" content="{{ implode(', ', array_slice($related, 0, 10)) }}">
@endsection


@section('content')
<div class="content">
	<div class="article">
		<header class="main-header">
			<div id="header">
				<a href="{{ url('/') }}" rel="nofollow" title="Resume Example">
				<h1>{{ strtoupper($result[0]) }}</h1></a>
			</div>
		</header>


		<div class="headertext">
			<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<span typeof="v:Breadcrumb"><a href="{{ url('/') }}" property="v:title" rel="v:url">Home</a></span>
			</div>
		</div>

		<div>

		</div>

		<ul>
		
			@foreach ($result as $data)
			<li>
				<a alt="{{ ucwords(str_replace('-', ' ', $data)) }}" href="{{ url(str_slug($data)) }}" title="{{ ucwords(str_replace('-', ' ', $data)) }}">{{ substr(ucwords(str_replace('-', ' ', $data)), 0, 30) }}</a>
			</li>

			@endforeach

		</ul>
		<div class="clear"></div>
		<div id="pagination"><a href="{{ ('/') }}" title="First page">« First</a>...<b>1</b>
		@for ($i=2; $i<13; $i++)
			<a href="{{ ('/'.$i) }}" title="Page {{$i}}">{{$i}}</a>
		@endfor
		<a href="/13" title="Page 13">13</a>...<a href="/13" title="Last page">Last »</a></div>

	</div>
	<!-- Start Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div id="logo2">
		</div>
	</aside>
	<!-- End Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div class="sidebarmenunavigation">
		</div>
	</aside>


	<aside class="sidebar walleft1">
		
				<div id="sidebars" class="sidebar">
					<div class="sidebar_list">
						<ul class="rand-text">						
							@foreach ($related as $rel)
								<ul class="popular-posts">
									<li><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}">{{ ucwords($rel) }}</a><div class="sidebartextviews">» {{ rand(1000,3000) }}  views</div></li>
								</ul>
							@endforeach
						</ul>
					
						<div style="clear: both"></div>
					</div>
					<div class="ads_sidebar"><!--ads--></div>
				</div>	
			</aside>
</div>
@endsection