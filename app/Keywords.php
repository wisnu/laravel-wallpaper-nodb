<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keywords extends Model
{
    protected $connection = 'mysql';
    protected $table = 'keywords';
    public $timestamps = false;
    protected $fillable = ['status', 'keyword', 'hash'];

}
