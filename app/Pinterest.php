<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinterest extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pinterest';
    public $timestamps = false;
//     protected $fillable = ['status', 'keyword'];
}
