<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
		'App\Console\Commands\Fetch',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
// 			$schedule->command('fetch imageDB')->everyMinute();




        // $schedule->command('inspire')
        //          ->hourly();
//      		$schedule->command('fetch getkeyword --sort=170.10.161.251')->everyMinute();
     		$schedule->command('fetch getkeyword --sort=170.10.161.251')->everyMinute();
     		$schedule->command('fetch getkeyword --sort=pool.wis.nu/wallpaper')->everyMinute();
     		$schedule->command('fetch getkeyword --sort=haloweenhike.com')->everyMinute();


/*
     		$schedule->command('fetch image --sort=asc')->everyMinute();
     		$schedule->command('fetch image --sort=desc')->everyMinute();
*/
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
