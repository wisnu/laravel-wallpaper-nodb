<?php

namespace App\Console\Commands;

use \Browser\Casper;
use App\Keywords;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Moloquent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;
use Torann\GeoIP\GeoIPFacade as GeoIP;


class Fetch extends Command
{
	//     private static $casperBinPath = '/usr/local/bin/';
	private static $casperBinPath = '/usr/local/bin/';
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'fetch {exe=now : now/image/toDB} {--F|file=xaa : Filenya} {--S|sort=asc : sorted by}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';
	/*
        $filename = '/tmp/casperjs-test.png';
        $casper = new Casper(self::$casperBinPath);
*/

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$file = fopen(Storage::disk('local')->url('keywords.txt'), 'r');
		$data = array();
		while (($line = fgetcsv($file)) !== FALSE) {
			//       $result[] = array(md5($line[0]) => $line[0]);
			$data[] = $line[0];
		}
		
// 		shuffle($this->file);

		$this->data = $data;

	}

	protected $file, $manager;




	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		// mandatory command
		if ($this->argument('exe') == 'now') {
			$this->now();
		}

		if ($this->argument('exe') == 'image') {
			$this->imageGetnoDB($this->option('file'));
		}


		
		// helpers

		if ($this->argument('exe') == 'todb') {
			$this->toDB($this->option('file'));
		}

		if ($this->argument('exe') == 'tes') {
			$this->tes();
		}

		if ($this->argument('exe') == 'imageDB') {
			$this->imageGet();
		}


		if ($this->argument('exe') == 'getkeyword') {
			$this->getkeyword($this->option('sort'));
		}

	}

	public function now() {
		foreach ($this->data as $data) {
			$cachekey = md5($data);
			$this->info($data);
			\Log::info($data);

			if (Cache::has($cachekey)) {
				$result = Cache::get($cachekey);
				Storage::disk('local')->put('json/'.$cachekey.'.json', $result);
			} else {
	            $result = file_get_contents('http://pool.wis.nu/wallpaper/tes2/'.urlencode($data));
	          	Cache::put($cachekey.'.json', $result, 1440);
	            $result = Cache::get($cachekey.'.json');
				Storage::disk('local')->put('json/'.$cachekey.'.json', $result);
			}
		}
	}


	public function imageGetnoDB($kfile) {

		$file = fopen(Storage::url($kfile), 'r');
		$data = array();
		while (($line = fgetcsv($file)) !== FALSE) {
			//       $result[] = array(md5($line[0]) => $line[0]);
			$data[] = $line[0];
		}

		foreach ($data as $line[0]) {

			$hash = md5($line[0]);
			$problems = array("soresearchplant.com", "captaincookandthecoconutz.com", "rarerecruitment.co.uk");

			$this->info('start  '.$line[0]);

			$result = json_decode(Storage::disk('local')->get('json/'.$hash.'.json'), true);
			\Log::info('start  '.$hash);

			foreach ($result['data'] as $res) {
				$xraw = strpos((string)$res['url'], (string)'x-raw-image');

				if ($xraw === false) {

				} else {
					$array['data'] = $this->removeElementWithValue((array)$result['data'], 'gid', $res['gid']);
					Storage::disk('local')->put('json/'.$hash.'.json', json_encode($array), 'public');
				}
				foreach ($problems as $problem) {
					$xdom = strpos((string)$res['url'], (string)$problem);
					if ($xdom === false) {
					} else {
						$array['data'] = $this->removeElementWithValue((array)$result['data'], 'gid', $res['gid']);
						Storage::disk('local')->put('json/'.$hash.'.json', json_encode($array), 'public');
					}
				}
			}


			$result = json_decode(Storage::disk('local')->get('json/'.$hash.'.json'), true);
			$notOK = array(); // collecting error gid
			foreach ($result['data'] as $res) {
				$imgName = $res['slug'].'.jpg';
				$io = strpos($res['url'], '.wp.com');

				if ($io === false) {
					$img_url = str_replace('http://', 'http://i0.wp.com/', str_replace('https://', 'https://i0.wp.com/', $res['url']));
				} else {
					$img_url = $res['url'];
				}

				// Lets combine not 200 gids


				if ($this->hc($img_url) != '200' ) {
					//      $this->comment($this->hc($img_url).' - '. $imgName);
					$notOK[] = $res['gid'];
				} elseif (!$this->hc($img_url)) {
					$notOK[] = $res['gid'];
				}
			}


			foreach ($notOK as $no) {
				//     $this->error('fixing '.$hash);
				\Log::info('fixing  '.$hash);
				$newresult = json_decode(Storage::disk('local')->get('json/'.$hash.'.json'), true);

				$array['data'] = $this->removeElementWithValue((array)$newresult['data'], 'gid', $no);

				//     Storage::disk('spaces')->delete('json/'.$hash.'.json');

				Storage::disk('local')->put('json/'.$hash.'.json', json_encode($array), 'public');
			}


			// Get image with new filtered data

			$finalresult = json_decode(Storage::disk('local')->get('json/'.$hash.'.json'), true);
			foreach ($finalresult['data'] as $res) {
				$imgName = $res['slug'].'.jpg';
				$io = strpos($res['url'], '.wp.com');

				if ($io === false) {
					$img_url = str_replace('http://', 'http://i0.wp.com/', str_replace('https://', 'https://i0.wp.com/', $res['url']));
				} else {
					$img_url = $res['url'];
				}
				
				// if image not exist then save it
				if (Storage::disk('local')->has('images/'.$imgName)) {
					$this->comment('File exist for '.$imgName);
				} else {
					$this->line('Saving '.$imgName);
					$file = Storage::disk('local')->put('images/'.$imgName, file_get_contents($img_url), 'public');
				}
			}

		}
	}







	public function getkeyword($sort) {
		/*
		$keywords = Keywords::where('status', 'inserted')
		->orderBy('keyword', $sort)
		->take(40)
		->get();
*/
		$keywords = new Keywords;
		$keywords = $keywords->where('status', 'inserted')->inRandomOrder()->take(40)->get();

		foreach ($keywords as $keyword) {
			\Log::info('fetching '.$keyword->hash);

			try {
				$download = Storage::disk('spaces')->put('json/'.$keyword->hash.'.json', file_get_contents('http://'.$sort.'/tes2/'.urlencode($keyword->keyword)), 'public');

			} catch (Exception $e) {
				\Log::error('error json '.$keyword->hash);
				Storage::disk('spaces')->put('json/'.$keyword->hash.'.json', file_get_contents('http://'.$sort.'/tes2/'.urlencode($keyword->keyword)), 'public');

			}
			if (Storage::disk('spaces')->exists('json/'.$keyword->hash.'.json')) {
				$keyword->status = 'fetched';
				$keyword->save();
			}
		}
	}



	public function todb($kfile) {
		$this->comment($kfile);
		$file = fopen(Storage::disk('local')->url($kfile), 'r');
		$data = array();
		while (($line = fgetcsv($file)) !== FALSE) {
			//       $result[] = array(md5($line[0]) => $line[0]);
			$data[] = $line[0];
		}

		foreach ($data as $data) {
			$keywords = new Keywords;
			$keywords->hash = md5($data);
			$keywords->keyword = $data;
			$keywords->status = 'inserted';
			$keywords->save();
			$this->info($data);

		}
	}





	public function imageGet() {
		sleep(rand(1,15));
		$keyword = json_decode(file_get_contents('http://halloweenhike.com/random.html'));
		$hash = $keyword->hash;
		$problems = array("ramazancalay.com", "captaincookandthecoconutz.com", "rarerecruitment.co.uk");


		$result = json_decode(Storage::disk('spaces')->get('json/'.$keyword->hash.'.json'), true);
		\Log::info('start  '.$keyword->hash);

		foreach ($result['data'] as $res) {
			$xraw = strpos((string)$res['url'], (string)'x-raw-image');

			if ($xraw === false) {

			} else {
				$array['data'] = $this->removeElementWithValue((array)$result['data'], 'gid', $res['gid']);
				Storage::disk('spaces')->put('json/'.$keyword->hash.'.json', json_encode($array), 'public');
			}
			foreach ($problems as $problem) {
				$xdom = strpos((string)$res['url'], (string)$problem);
				if ($xdom === false) {
				} else {
					$array['data'] = $this->removeElementWithValue((array)$result['data'], 'gid', $res['gid']);
					Storage::disk('spaces')->put('json/'.$keyword->hash.'.json', json_encode($array), 'public');
				}
			}
		}


		$result = json_decode(Storage::disk('spaces')->get('json/'.$keyword->hash.'.json'), true);
		$notOK = array(); // collecting error gid
		foreach ($result['data'] as $res) {
			$imgName = $res['slug'].'.jpg';
			$io = strpos($res['url'], '.wp.com');

			if ($io === false) {
				$img_url = str_replace('http://', 'http://i0.wp.com/', str_replace('https://', 'https://i0.wp.com/', $res['url']));
			} else {
				$img_url = $res['url'];
			}

			// Lets combine not 200 gids


			if ($this->hc($img_url) != '200' ) {
				//      $this->comment($this->hc($img_url).' - '. $imgName);
				$notOK[] = $res['gid'];
			} elseif (!$this->hc($img_url)) {
				$notOK[] = $res['gid'];
			}
		}


		foreach ($notOK as $no) {
			//     $this->error('fixing '.$keyword->hash);
			\Log::info('fixing  '.$keyword->hash);
			$newresult = json_decode(Storage::disk('spaces')->get('json/'.$keyword->hash.'.json'), true);

			$array['data'] = $this->removeElementWithValue((array)$newresult['data'], 'gid', $no);

			//     Storage::disk('spaces')->delete('json/'.$keyword->hash.'.json');

			Storage::disk('spaces')->put('json/'.$keyword->hash.'.json', json_encode($array), 'public');
		}


		// Get image with new filtered data

		$finalresult = json_decode(Storage::disk('spaces')->get('json/'.$keyword->hash.'.json'), true);
		foreach ($finalresult['data'] as $res) {
			$imgName = $res['slug'].'.jpg';
			$io = strpos($res['url'], '.wp.com');

			if ($io === false) {
				$img_url = str_replace('http://', 'http://i0.wp.com/', str_replace('https://', 'https://i0.wp.com/', $res['url']));
			} else {
				$img_url = $res['url'];
			}

			$file = Storage::disk('spaces')->put('images/'.$imgName, file_get_contents($img_url), 'public');

		}


		shell_exec("curl -s 'http://halloweenhike.com/update/$hash.html'");
	}

	public function tes() {
		$result = json_decode(Storage::disk('spaces')->get('json/380ad00a9008b9e6b6ea7da495047251.json'), true);
		foreach ($result['data'] as $res) {
			$io = strpos($res['url'], '.wp.com');

			if ($io === false) {
				$img_url = str_replace('http://', 'http://i0.wp.com/', str_replace('https://', 'https://i0.wp.com/', $res['url']));
			} else {
				$img_url = $res['url'];
			}

			$this->line($img_url);
		}
	}

	public function removeElementWithValue($array, $key, $value){
		foreach($array as $subKey => $subArray){
			if($subArray[$key] == $value){
				unset($array[$subKey]);
			}
		}
		return $array;
	}



	public function hc($url) {
		ini_set("user_agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
		stream_context_set_default( ['ssl' => ['verify_peer' => false, 'verify_peer_name' => false, ],]);
		ini_set('default_socket_timeout', 10);
		$headers = get_headers($url, 1);
		if ($headers) {
			return substr($headers[0], 9, 3);
		} else {
			return '500';
		}
	}

	public function chk($res) {
		try {
			$check = get_headers($res->url, 1);
		} catch (ConnectException $er) {
			dd($er);
		}
		return $check;
	}

	public function saveImg2($res, $imgName) {
		$manager = new ImageManager(array('driver' => 'imagick'));
		ini_set("user_agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
		stream_context_set_default( ['ssl' => ['verify_peer' => false, 'verify_peer_name' => false, ],]);

		$check = $this->chk($res);

		if ($check[0] == 'HTTP/1.1 404 Not Found' or $check[0] == 'HTTP/1.1 400 Bad Request' or $check[0] == 'HTTP/1.0 504 Gateway Timeout'  or $check[0] == 'HTTP/1.1 504 Gateway Timeout') {
			$new_url = 'http://via.placeholder.com/500/fff?text=SAMPLE+RESUME';
		} elseif ($check[0] == 'HTTP/1.1 301 Found' or $check[0] == 'HTTP/1.0 301 Moved Permanently') {
			$new_url = $check['Location'];
		} elseif ($check[0] == 'HTTP/1.1 200 OK') {
			$new_url = $res->url;
			/*
		} elseif (!$check) {
			$new_url = 'http://via.placeholder.com/500/fff?text=SAMPLE+RESUME';
*/
		} else {
			$new_url = 'http://via.placeholder.com/500/fff?text=SAMPLE+RESUME';
		}




		$casper = new Casper(self::$casperBinPath);
		$casper->setOptions([
			'ignore-ssl-errors' => 'yes'
			]);
		//   $casper->setViewPort($res->width, $res->height);
		$casper->download($res->visibleUrl, $new_url, Storage::disk('local')->url('public/'.$imgName));
		$casper->run();





		$img = $manager->make(Storage::disk('local')->url('public/'.$imgName));
		$img->getCore()->stripImage();

		$img->save(Storage::disk('local')->url('public/'.$imgName), 90);

	}
	public function getUrl($img_url) {
		stream_context_set_default( ['ssl' => ['verify_peer' => false, 'verify_peer_name' => false, ],]);
		$check = get_headers($img_url, 1);
		//   print_r($check);
		if ($check[0] == 'HTTP/1.1 200 OK') {
			$new_url = $img_url;
		} else {
			$new_url = 'http://via.placeholder.com/500/fff?text=SAMPLE+RESUME';
		}
		return $new_url;
		/*
		$ch = curl_init();
		stream_context_set_default( ['ssl' => ['verify_peer' => false, 'verify_peer_name' => false, ],]);

		// set url
		curl_setopt($ch, CURLOPT_URL, $new_url);

		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_REFERER, 'https://google.com');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		// $output contains the output string
		$output = curl_exec($ch);
		return $output;
 		curl_close($ch);
*/

	}
	public function saveImg($img_url, $imgName) {
		$manager = new ImageManager(array('driver' => 'imagick'));
		$output = $this->getUrl($img_url);

		$img = $manager->make($output);
		$img->getCore()->stripImage();


		$img->save(Storage::disk('local')->url('public/'.$imgName), 90);



	}

	public function getSSLPage($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSLVERSION,3);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}


}