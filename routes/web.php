<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('tes.htm', 'Controller@tes');
Route::get('random.html', 'ToolController@random');
Route::get('update/{id}.html', 'ToolController@update');

Route::get('img/{path}', function($path, League\Glide\Server $server, Illuminate\Http\Request $request) {
    $server->outputImage($path, $request->all());
})->where('path', '.+');



Route::get('/{page?}', 'Controller@index')->name('home')->where('page', '[1-9]+[0-9]*');
Route::get('{slug}/{page?}', 'Controller@single')->where('page', '[1-9]+[0-9]*');
/*
Route::get('{verify}.html', function($verify)
	{
		$verify = Config::get('money.verify');
		return "google-site-verification: $verify.html";
	});
*/
Route::get('page/{slug}.html', 'Controller@page');
Route::get('{slug}/{permalink}.html', 'Controller@attachment');
Route::get('tes2/{keyword}', 'ToolController@casper');
